    module  random_seed_mod


    contains

        subroutine  random_seed_sub()
                integer :: i, n, clock
                integer, dimension(:), allocatable :: seed

            call random_seed(size = n)
            allocate(seed(n))

            call system_clock(count=clock)

            seed = clock + 37 * (/ (i - 1, i = 1, n) /)
            call random_seed(put = seed)

            deallocate(seed)

    end subroutine  random_seed_sub


    end module  random_seed_mod
