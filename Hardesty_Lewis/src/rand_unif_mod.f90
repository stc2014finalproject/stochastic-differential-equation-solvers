    module  rand_unif_mod
! [Comments]


    contains

        subroutine  rand_unif(n,rand,a,b)
            use random_seed_mod
                integer, intent(in)  ::  n
                integer, parameter   ::  dp = selected_real_kind(p=15)
                real(kind=dp), dimension(n), intent(out)  ::  rand
                integer, intent(in)                   ::  a,b

            call random_seed_sub()
            call random_number(rand)
            rand  =  rand * (real(b)-real(a)) + real(a)

        end subroutine  rand_unif


    end module  rand_unif_mod
