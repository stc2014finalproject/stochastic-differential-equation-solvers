    module  integ_unbias_mod
![Comments]


    contains

        subroutine  integ_unbias_mean(sm1,s,n,integral)
            use rand_unif_mod
                implicit none
                integer, parameter   ::  dp = selected_real_kind(p=15)
                integer, parameter   ::  total_pts = 100000
                integer, intent(inout)  ::  sm1,s
                integer, intent(in)        ::  n
                real(kind=dp), dimension(total_pts,n)     ::  mean_s
                real(kind=dp), dimension(n)  ::  integrand_sum,      &
                                                 mean_integrand_sum
                real(kind=dp), dimension(n), intent(out)  ::  integral
                real(kind=dp), dimension(total_pts)  ::  rand_x
                integer              ::  l


            call rand_unif(total_pts,rand_x,sm1,s)
            mean_s(:,1)  =  rand_x * 2._dp
!            call sleep(1)
            call rand_unif(total_pts,rand_x,sm1,s)
            mean_s(:,2)  =  rand_x * 0.1_dp

            integrand_sum  =  0._dp
            summing:  do  l = 1,total_pts
                integrand_sum  =  integrand_sum + mean_s(l,:)
            end do  summing
            mean_integrand_sum  =  integrand_sum / real(total_pts)
            integral  =  mean_integrand_sum * (s-sm1)

        end subroutine  integ_unbias_mean

        subroutine  integ_unbias_gam(sm1,s,n,integral)
            use rand_unif_mod
                implicit none
                integer, parameter   ::  dp = selected_real_kind(p=15)
                integer, parameter   ::  total_pts = 100000
                integer, intent(inout)  ::  sm1,s
                integer, intent(in)        ::  n
                real(kind=dp), dimension(total_pts,n,n)  ::  cov_s,gam_s
                integer                        ::  i,j,k
                real(kind=dp), dimension(n,n)  ::  integrand_sum,      &
                                                   mean_integrand_sum
                real(kind=dp), dimension(n,n), intent(out)  ::  integral
                real(kind=dp), dimension(total_pts)  ::  rand_x
                integer              ::  l

            call rand_unif(total_pts,rand_x,sm1,s)
            cov_s(:,1,1)  =  1._dp
!            call sleep(1)
            call rand_unif(total_pts,rand_x,sm1,s)
            cov_s(:,2,1)  =  0.3_dp
!            call sleep(1)
            call rand_unif(total_pts,rand_x,sm1,s)
            cov_s(:,1,2)  =  cos(rand_x) ** 2._dp * 0.1_dp + 0.5_dp
!            call sleep(1)
            call rand_unif(total_pts,rand_x,sm1,s)
            cov_s(:,2,2)  =  2._dp

            gam_s  =  0._dp
            do  j = 1,n
                do  i = 1,n
                    do  k = 1,n
                        gam_s(:,i,j)  =  gam_s(:,i,j) +              &
                                         cov_s(:,i,k) * cov_s(:,j,k)
                    end do
                end do
            end do

            integrand_sum  =  0._dp
            summing:  do  l = 1,total_pts
                integrand_sum  =  integrand_sum + gam_s(l,:,:)
            end do  summing
            mean_integrand_sum  =  integrand_sum / real(total_pts)
            integral  =  mean_integrand_sum * (real(s)-real(sm1))

        end subroutine  integ_unbias_gam


    end module  integ_unbias_mod
