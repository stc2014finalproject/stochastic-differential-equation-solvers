    program  rand_normal
        use random_seed_mod
            implicit none
![Comments]
            integer, parameter         ::  dp = selected_real_kind(p=15)
            integer, parameter         ::  npoints_sought = 1000
            integer, parameter         ::  npoints_tested = npoints_sought*16
            real(kind=dp), parameter   ::  a = -5._dp,b = 5._dp
            integer, parameter         ::  numb_files = 2
            integer, parameter         ::  filepref_len = 11,filesuff_len = 4
            character(len=filepref_len), parameter    ::  filepref='rand_normal'
            character(len=filesuff_len), parameter    ::  filesuff ='.dat'

            integer                    ::  openstat,writestat,closestat, &
                                           i,j
            real(kind=dp), dimension(npoints_tested)  ::  rand_y,rand_u,u_comp
            real(kind=dp), dimension(npoints_sought)  ::  rand_x
            character(len=2)           ::  filenum_form_pref
            character(len=1)           ::  filenum_form_num,filenum_form_suff
            character(len=4)           ::  filenum_form
            integer, parameter         ::  n_digits = ceiling(log10(real(numb_files)))+1
            integer                    ::  k,l
            character(len=n_digits+1)  ::  filenum
            integer, parameter         ::  file_len = n_digits+1+filepref_len+filesuff_len
            character(len=file_len)    ::  nameoffile


        filenum_form_pref  =  '(I'
        write(filenum_form_num,'(I1)') n_digits+1
        filenum_form_suff  =  ')'
        filenum_form  =  filenum_form_pref // filenum_form_num // filenum_form_suff

        creating:  do  k = 1,numb_files

            l  =  10 ** n_digits + k
            write(filenum,filenum_form) l
            nameoffile  =  filepref // filenum // filesuff

            call random_seed_sub()
            call random_number(rand_y)
            rand_y  =  rand_y * (b-a) + a
            call random_seed_sub()
            call random_number(rand_u)
            u_comp  =  exp((rand_y**2._dp)*(-1._dp)/2._dp)

            j  =  0
            open(unit=k,file=nameoffile,status='replace',iostat=openstat, &
                 access='sequential',form='formatted',action='readwrite', &
                 position='rewind',delim='none',pad='yes')
            check_open:  if(openstat==0) then
                accum:  do  i = 1,npoints_tested
                    accept_reject:  if(rand_u(i)<=u_comp(i)) then
                        rand_x(i-j)  =  rand_y(i)
                        write(unit=k,fmt=*,iostat=writestat) rand_x(i-j)
                        check_write:  if(writestat/=0) then
                            write(*,*) 'Write failed on ',nameoffile,'.'
                            stop
                        end if  check_write
                        j  =  j + 1
                        accum_complete:  if(j==npoints_sought) then
                            exit
                        end if  accum_complete
                    end if  accept_reject
                end do  accum
            else
                write(*,*) nameoffile,' failed to open.'
                stop
            end if  check_open

            close(unit=k,status='keep',iostat=closestat)
            check_close:  if(closestat/=0) then
                write(*,*) nameoffile,' failed to close.'
                stop
            end if  check_close

        end do  creating


    end program  rand_normal
