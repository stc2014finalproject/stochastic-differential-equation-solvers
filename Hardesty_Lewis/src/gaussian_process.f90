    program  gaussian_process
        use integ_unbias_mod
            implicit none
![Comments]
            integer, parameter             ::  dp = selected_real_kind(p=15)
            integer, parameter             ::  step_max = 1000,step_size = 1,d = 2
            integer, parameter             ::  numb_proc = 1,numb_files = d
            integer, parameter             ::  filpref_len = 16,filsuff_len = 4,  &
                                               filepref_len = 11,filesuff_len = 4
            character(len=filpref_len), parameter   ::  filpref = 'gaussian_process'
            character(len=filsuff_len), parameter   ::  filsuff = '.dat'
            character(len=filepref_len), parameter  ::  filepref = 'rand_normal'
            character(len=filesuff_len), parameter  ::  filesuff = '.dat'


            real(kind=dp), dimension(d)    ::  mean_t,integ_mean_t,x,               &
                                               rand_norm,prod_integ_rand,rand_gauss
            real(kind=dp), dimension(d,d)  ::  integ_gam_t
            integer                        ::  tm1,t,mm
            integer                        ::  openstat0,writestat0a,writestat0b,closestat0, &
                                               openstat1,readstat1,closestat1,               &
                                               p,q,i,pos_sought,pos_tested

            character(len=2)               ::  filnum_form_pref
            character(len=1)               ::  filnum_form_num,filnum_form_suff
            character(len=4)               ::  filnum_form
            integer, parameter             ::  m_digits = ceiling(log10(real(numb_proc)))+1
            integer                        ::  m,n
            character(len=m_digits+1)      ::  filnum
            integer, parameter             ::  fil_len = m_digits+1+filpref_len+filsuff_len
            character(len=fil_len)         ::  nameoffil

            character(len=2)               ::  filenum_form_pref
            character(len=1)               ::  filenum_form_num,filenum_form_suff
            character(len=4)               ::  filenum_form
            integer, parameter             ::  n_digits = ceiling(log10(real(numb_files)))+1
            integer                        ::  k,l
            character(len=n_digits+1)      ::  filenum
            integer, parameter             ::  file_len = n_digits+1+filepref_len+filesuff_len
            character(len=file_len)        ::  nameoffile


        filnum_form_pref  =  '(I'
        write(filnum_form_num,'(I1)') m_digits+1
        filnum_form_suff  =  ')'
        filnum_form  =  filnum_form_pref // filnum_form_num // filnum_form_suff

        filenum_form_pref  =  '(I'
        write(filenum_form_num,'(I1)') n_digits+1
        filenum_form_suff  =  ')'
        filenum_form  =  filenum_form_pref // filenum_form_num // filenum_form_suff

        pos_sought  =  int(real(step_max)/real(step_size))

        x  =  (/1._dp,0.5_dp/)

        do  m = 1,numb_proc

            tm1  =  0

            p  =  m * 2
            q  =  m * 2 + 1

            n  =  10 ** m_digits + m
            write(filnum,filnum_form) n
            nameoffil  =  filpref // filnum // filsuff

            open(unit=p,file=nameoffil,status='replace',iostat=openstat0, &
                 access='sequential',form='formatted',action='readwrite', &
                 position='rewind',delim='none',pad='yes')

            write(unit=p,fmt=*,iostat=writestat0a) tm1,x

            do  t = 1,step_max,step_size

                mm  =  t
                call integ_unbias_mean(tm1,mm,d,integ_mean_t)
                call integ_unbias_gam(tm1,mm,d,integ_gam_t)

                do  k = 1,numb_files

                    l  =  10 ** n_digits + k
                    write(filenum,filenum_form) l
                    nameoffile  =  filepref // filenum // filesuff

                    open(unit=q,file=nameoffile,status='old',iostat=openstat1,    &
                         access='sequential',form='formatted',action='readwrite', &
                         position='rewind',delim='none',pad='yes')
                    pos_tested  =  int(real(t)/real(step_size))

                    do  i = 1,pos_sought
                        read(unit=q,fmt=*,iostat=readstat1) rand_norm(k)
                        if(i>=pos_tested) exit
                    end do

                    close(unit=q,status='keep',iostat=closestat1)

                end do

                prod_integ_rand  =  0._dp
                call dgemm('n','n',d,1,d,1._dp,integ_gam_t,d,rand_norm,d,0._dp,prod_integ_rand,d)
                rand_gauss  =  prod_integ_rand + integ_mean_t
                write(unit=p,fmt=*,iostat=writestat0b) t,rand_gauss

                tm1  =  t

            end do

            close(unit=p,status='keep',iostat=closestat0)

        end do


    end program  gaussian_process
