#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<mpi.h>
#define N 100000

double integral_2t(double x_start, double x_end);
double integral_01t(double x_start, double x_end);
double integral1(double x_start, double x_end);
double integral2(double x_start, double x_end);
double standardnormal();

int main(int argc, char *argv[])
{
   int myrank, size;
   int localstart, localend;
   double localsum1=0,sum1=0, localsum2=0,sum2=0;
   FILE *p;
   FILE *p1;
   int i = 0;
   double x[2][N];
   double diff[2][1];
   double sigma[2][2];
   double mean[2][1];
   double standard[2][1];
   double t[N];
   double h = 10.0/N;
   x[0][0] = 1;
   x[1][0]=0.5;
   t[0] = 0;
   
   // Initilize Mpi and local rank and size

   MPI_Init(&argc, &argv);

   MPI_Comm_rank(MPI_COMM_WORLD, &myrank);

   MPI_Comm_size(MPI_COMM_WORLD, &size);

   //Different MPI Tasks are doing different jobs!!!

   localstart=N/size * myrank +1; 

   localend=N/size * (myrank +1);

   for(i = 1; i < N; i++)
   {
     t[i] = t[i-1] + h;
	 mean[0][0] = integral_2t(t[i-1], t[i]);
	 mean[1][0] = integral_01t(t[i-1], t[i]);
	 sigma[0][0] = integral2(t[i-1], t[i]);
	 sigma[1][0] = integral2(t[i-1], t[i]);
	 sigma[0][1] = integral2(t[i-1], t[i]);
	 sigma[1][1] = 4.09 * h;
	 standard[0][0] = standardnormal();
	 standard[1][0]= standardnormal();
	 diff[0][0] = mean[0][0] + sigma[0][0] * standard[0][0] + sigma[0][1] * standard[1][0];
	 diff[1][0] = mean[1][0] + sigma[1][0] * standard[0][0] + sigma[1][1] * standard[1][0];
	 x[0][i] = x[0][i-1] + diff[0][0];
	 x[1][i] = x[1][i-1] + diff[1][0];
   }
   
   for(i = localstart; i <=localend; i++)
   {
     localsum1+=x[0][i];
	 localsum2+=x[1][i];
   }
   
   MPI_Reduce(&localsum1, &sum1, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
   MPI_Reduce(&localsum2, &sum2, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
   if(myrank==0)
 
   printf("\nThe mean vector is (%f , %f)\n", sum1/N, sum2/N );
   MPI_Finalize();
    
  return 0;

}

double integral_2t(double x_start, double x_end)
{
   return (x_end * x_end - x_start * x_start);
}

double integral_01t(double x_start, double x_end)
{

    return (x_end * x_end - x_start * x_start) * 0.05;

}

double integral1(double x_start, double x_end)
{
   double m,n;
   m = 1.3 * x_start + 0.1 * (x_start + sin(x_start) * cos(x_start));
   n = 1.3 * x_end + 0.1 * (x_end + sin(x_end) * cos(x_end));
   return n-m;
}


double integral2(double x_start, double x_end)
{
   double m,n;
   m = 1.25 * x_start + 0.01/32 * (12 * x_start + 8 * sin(2 * x_start) + sin(4 * x_start)) + 0.05 * (x_start + sin(x_start) * cos(x_start));
   n = 1.25 * x_end + 0.01/32 * (12 * x_end + 8 * sin(2 * x_end) + sin(4 * x_end)) + 0.05 * (x_end + sin(x_end) * cos(x_end));
   return n-m;
}

double standardnormal()
{
  double a;
  double b,c;
  int logic = 0;
  
    while(logic != 1)
	{
	    b=rand() * 10.0/RAND_MAX - 5 ;
        c=rand() * 1.0/RAND_MAX;
	if(c <= exp(-(b*b/2)))
	{
           logic = 1;
           a = b; 	  
	}
	}
 return a;
}
