#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#define N 10
#define diff(i,j) diff[(i) + (j)*n] //difference vector
#define x(i,j) x[(i) + (j)*N]   //stochastic process vector
#define sigma(i,j) sigma[(i) + (j)*m]   //variance matrix
#define mean(i,j) mean[(i) + (j)*n] //mean vector
#define standard(i,j) standard[(i) + (j)*n] //standardnoral vector
double integral_2t(double x_start, double x_end);
double integral_01t(double x_start, double x_end);
double integral1(double x_start, double x_end);
double integral2(double x_start, double x_end);
double standardnormal();

int main()
{
   FILE *p;
   FILE *p1;
   int n = 1;
   int m = 2;
   int i = 0;
   double alpha = 1.0;
   double beta = 0.0;
   char trans[1]={'N'};
   char transa[1]={'N'};
   char transb[1]={'N'};
   
   double *x=malloc(m*N*sizeof(double)); 
   double *diff=malloc(m*n*sizeof(double)); 
   double *sigma=malloc(m*m*sizeof(double));
   double *standard=malloc(m*n*sizeof(double));
   double *mean=malloc(m*n*sizeof(double)); 

   double t[N];
   double h = 10.0/N;
   x(0,0) = 1;
   x(1,0)=0.5;
   t[0] = 0;
   
   if((p=fopen("project.dat","wb"))==NULL){
      printf("\nUnable to open file myfile.dat");
      exit(1);
  }

   
   if((p1=fopen("point.dat","wb"))==NULL){
      printf("\nUnable to open file myfile.dat");
      exit(1);
  }

   for(i = 1; i < N; i++)
   {
     
     t[i] = t[i-1] + h;
	 mean(0,0) = integral_2t(t[i-1], t[i]);
	 mean(1,0) = integral_01t(t[i-1], t[i]);
	 sigma(0,0) = integral2(t[i-1], t[i]);
	 sigma(1,0) = integral2(t[i-1], t[i]);
	 sigma(0,1) = integral2(t[i-1], t[i]);
	 sigma(1,1) = 4.09 * h;
	 standard(0,0) = standardnormal();
	 standard(1,0)= standardnormal();
	 /*diff[0][0] = mean[0][0] + sigma[0][0] * standard[0][0] + sigma[0][1] * standard[1][0];
	 diff[1][0] = mean[1][0] + sigma[1][0] * standard[0][0] + sigma[1][1] * standard[1][0];*/
	 dgemm_(transa,transb,&m,&m,&n,&alpha,sigma,&m,standard,&m,&beta,diff,&m);
         diff(0,0) = diff(0,0) + mean(0,0);
         diff(0,1) = diff(0,1) + mean(0,1);	 
	 x(0,i) = x(0,i-1) + diff(0,0);
	 x(1,i) = x(1,i-1) + diff(1,0);
   }
   for(i = 0; i<N;i++)
   {
    fprintf(p, "X(%f) is (%f, %f)\n", t[i], x(0,i), x(1,i));
    fprintf(p1,"{%f, %f, %f},", x(0,i),x(1,i),t[i]);
   }
   
    
  fclose(p);
  fclose(p1);
  return 0;

}

// integral of 2t
double integral_2t(double x_start, double x_end)
{
   return (x_end * x_end - x_start * x_start);
}

//integral of 0.1t
double integral_01t(double x_start, double x_end)
{

    return (x_end * x_end - x_start * x_start) * 0.05;

}

// integral of the matrix of variance
double integral1(double x_start, double x_end)
{
   double m,n;
   m = 1.3 * x_start + 0.1 * (x_start + sin(x_start) * cos(x_start));
   n = 1.3 * x_end + 0.1 * (x_end + sin(x_end) * cos(x_end));
   return n-m;
}


double integral2(double x_start, double x_end)
{
   double m,n;
   m = 1.25 * x_start + 0.01/32 * (12 * x_start + 8 * sin(2 * x_start) + sin(4 * x_start)) + 0.05 * (x_start + sin(x_start) * cos(x_start));
   n = 1.25 * x_end + 0.01/32 * (12 * x_end + 8 * sin(2 * x_end) + sin(4 * x_end)) + 0.05 * (x_end + sin(x_end) * cos(x_end));
   return n-m;
}

// generate a number that is standard normal distributed
double standardnormal()
{
  double a;
  double b,c;
  int logic = 0;
  
    while(logic != 1)
	{
	    b=rand() * 10.0/RAND_MAX - 5 ;
        c=rand() * 1.0/RAND_MAX;
	if(c <= exp(-(b*b/2)))
	{
           logic = 1;
           a = b; 	  
	}
	}
 return a;
}
